//
//  PhotoCollectionViewCell.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import UIKit

//Collection view cell
class PhotoCollectionViewCell: UICollectionViewCell , ReuseIdentifierProtocol{
     @IBOutlet weak var photoImageView: UIImageView!
}

