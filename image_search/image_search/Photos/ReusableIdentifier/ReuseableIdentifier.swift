//
//  ReuseableIdentifier.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

public protocol ReuseIdentifierProtocol: class {
    //Get identifier from class
    static var defaultReuseIdentifier: String { get }
}

public extension ReuseIdentifierProtocol where Self: UIView {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
