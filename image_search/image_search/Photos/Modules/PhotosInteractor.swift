//
//  PhotosInteractor.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoInteractorInput: class {
    func fetchAllPhotosFromServiceManager(_ searchText: String, page: Int)
}

protocol PhotoInteractorOuput: class {
    func providePhotos(_ data:PhotoObject)
}

//Interactor
class PhotoInteractor: PhotoInteractorInput {
    
    var APIManager: FlickrPhotoSearchProtocol!
    var presenter: PhotoInteractorOuput!
    
    //gets the data from service layer and provides to the view
    func fetchAllPhotosFromServiceManager(_ searchText: String, page: Int) {
        APIManager.fetchPhotosFor(searchText: searchText, page: page) { (result, _) in
            switch result {
            case .success(let obj):
                self.presenter.providePhotos(obj)
                Log.console(debug: "success")
            case .failure(let errorMsg):
                Log.console(debug: "\(#function) error: \(errorMsg)")
            }
        }
    }
    
}
