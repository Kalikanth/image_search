//
//  PhotosPresenter.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoSearchPresenterInput: PhotosViewControllerOutput , PhotoInteractorOuput {
}

class PhotoPresenter: PhotoSearchPresenterInput {
    
    var interactor: PhotoInteractorInput!
    var view: PhotosViewControllerIntput!
    
    //Presenter says interactor ViewController needs photos
    func fetchPhotos(_ searchText: String, page: Int) {
        interactor.fetchAllPhotosFromServiceManager(searchText, page: page)
    }
    
    //Service return photos and interactor passes it to presenter
    func providePhotos(_ data:PhotoObject) {
        self.view.showFetchedImages(data)
    }

}
