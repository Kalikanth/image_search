//
//  PhotosInstanceAssembler.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

class PhotosInstanceAssember {
    
    static let sharedInstance = PhotosInstanceAssember()
    
    private init() {}
    //config
    func configure(_ viewController: PhotosViewController) {
        let APIManager = FlickerServiceManager()
        let interactor = PhotoInteractor()
        let presenter = PhotoPresenter()
        viewController.presenter = presenter
        presenter.view = viewController
        interactor.presenter = presenter
        presenter.interactor = interactor
        interactor.APIManager = APIManager
    }
    
}
