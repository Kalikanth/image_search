//
//  PhotosViewController.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import UIKit

protocol PhotosViewControllerOutput: class {
    func fetchPhotos(_ searchtag: String, page: Int)
}

protocol PhotosViewControllerIntput: class {
    func showFetchedImages(_ data:PhotoObject)
}

class PhotosViewController: UIViewController {
    
    //MARK : - ooutlets
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var inputTextField: UITextField!
    
    //Mark: - instance variables
    var presenter: PhotosViewControllerOutput!
    
    var photos: [PhotoModel] = []
    var currentPage = 1
    var totalPages = 1
    var isSearchTapped = false
    var photoSearchText = "kittens"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Configuring the class
        PhotosInstanceAssember.sharedInstance.configure(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //gets the images from server
         performSearchWith(photoSearchText)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //setups title to the navigation bar
        self.title = photoSearchText
    }

    //Request photo service result from Presenter
    func performSearchWith(_ searchText: String) {
        presenter.fetchPhotos(searchText, page: currentPage)
    }
    
    //searches the images with entered text
    @IBAction func searchTapped(_ sender: UIButton) {
        guard let text = self.inputTextField.text,
            !text.trim().isEmpty else {
                self.showAlert(message: "Enter search text")
                return
        }
        self.isSearchTapped = true
        self.photoSearchText = text
        self.title = photoSearchText
        self.performSearchWith(photoSearchText)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK:- PhotosViewControllerIntput protocol

extension PhotosViewController: PhotosViewControllerIntput {
    //Presenter returns us with photo results
    func showFetchedImages(_ data:PhotoObject) {
        if self.isSearchTapped {
            self.photos.removeAll()
        }
        
        self.photos = self.photos + data.photosData.photo
        self.totalPages = data.photosData.total.toInt
        DispatchQueue.main.async {
            self.photoCollectionView.reloadData()
        }
    }
}

// MARK:- UICollectionViewDataSource

extension PhotosViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //Photo cell + Loading cell
        if currentPage < totalPages {
            return photos.count + 1
        }
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < photos.count {
            return photoItemCell(collectionView, cellForItemAt: indexPath )
        } else {
            currentPage += 1
            performSearchWith(photoSearchText)
            return loadingItemCell(collectionView, cellForItemAt: indexPath)
        }
    }
    
    //collection view cell 
    func photoItemCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.defaultReuseIdentifier , for: indexPath as IndexPath) as! PhotoCollectionViewCell
        DispatchQueue.main.async {
            let photoUrl = self.photos[indexPath.row].photoUrl
            cell.photoImageView.getImageFrom(url: photoUrl) { (_) in  }
        }
        return cell
    }
    
    func loadingItemCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoLoaderCollectionViewCell.defaultReuseIdentifier, for: indexPath as IndexPath) as! PhotoLoaderCollectionViewCell
        
        return cell
    }
}

// MARK:- UICollectionViewDelegate
extension PhotosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //TODO:
    }
}

// MARK:- UICollectionViewDelegateFlowLayout
extension PhotosViewController: UICollectionViewDelegateFlowLayout {
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          var itemSize: CGSize
          let length = (UIScreen.main.bounds.width) / 3 - 1
          
          if indexPath.row < photos.count {
              itemSize = CGSize(width: length, height: length)
          } else {
              itemSize = CGSize(width: photoCollectionView.bounds.width, height: 50.0)
          }
          
          return itemSize
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 0.5
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 0.5
      }
}
