//
//  PhotoModel.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation

//Flicker image model
struct PhotoModel : Codable {
    let id: String
    let owner: String
    let farm: Int
    let secret: String
    let server: String
    let title: String
    let ispublic: Int
    let isfriend: Int
    let isfamily: Int
}

extension PhotoModel {
    //gets the formatted imaet url
    var photoUrl: URL {
        return flickrImageURL()
    }
    //formation of image url
    private func flickrImageURL() -> URL {
        return URL(string: "http://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg")!
    }
}
