//
//  PhotoObj.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation

//json top level object parsing
struct PhotoObject: Codable {
    let photosData: Pages
}
extension PhotoObject{
    private enum CodingKeys: String , CodingKey {
         case photosData = "photos"
    }
}

//json pages information parsing model
struct Pages: Codable {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let photo: [PhotoModel]
}
