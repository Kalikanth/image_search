//
//  ServiceError+handler.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

//service errors
enum ServiceErrors: Error {
    case invalidAccessErrorCode(msg: String) // 100
    case invalidRequest(msg: String) // 400
    case unauthorized(msg: String) // 401
    case cannotProcessData(msg: String) // 422
    case internalServerError(msg: String) // 500
    case badGateway(msg: String) // 502
    case serviceUnavailable(msg: String) // 503
    case gateWayTimeout(msg: String) // 504
    case unProcessableEntity(msg: String)
}

extension ServiceErrors {
    
    //gets the error message based on the status code
    static func handlerError(response: URLResponse?) -> ServiceErrors {
        guard let resp = response as? HTTPURLResponse else {
            return ServiceErrors.unProcessableEntity(msg: "Unprocessable entity")
        }
        
        switch resp.statusCode {
        case 100:
            UIViewController.topViewController()?.showAlert(message: "Api domain error")
            return self.invalidAccessErrorCode(msg: "Api domain error")
        case 400:
            UIViewController.topViewController()?.showAlert(message: "Invalid Request!")
            return self.invalidRequest(msg: "Invalid Request!")
        case 401:
            UIViewController.topViewController()?.showAlert(message: "Unauthorized request!")
            return self.unauthorized(msg: "Unauthorized request!")
        case 422:
            UIViewController.topViewController()?.showAlert(message: "Cannot process the data")
            return self.cannotProcessData(msg:  "Cannot process the data")
        case 500:
            UIViewController.topViewController()?.showAlert(message: "Internal Server Error")
            return self.cannotProcessData(msg: "Internal Server Error")
        case 502:
            UIViewController.topViewController()?.showAlert(message: "Bad Gateway")
            return self.cannotProcessData(msg: "Bad Gateway")
        case 503:
            UIViewController.topViewController()?.showAlert(message: "Service unavailable")
            return self.cannotProcessData(msg: "Service unavailable")
        default:
            UIViewController.topViewController()?.showAlert(message: "Unprocessable entity")
            return self.unProcessableEntity(msg: "Unprocessable entity")
        }
        
    }
}
