//
//  FlcikerServiceManager.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

/// Service manager protocol
protocol FlickrPhotoSearchProtocol: class {
    func fetchPhotosFor(searchText: String, page: Int, completion: @escaping(Result<PhotoObject, ServiceErrors> , _ code:Int) -> Void) -> Void
}

/// Service manager
class FlickerServiceManager {
    
    struct APIMetadataKeys {
        static let failureStatusCode = "code"
    }
    
    struct FlickrAPI {
        static let APIKey = "06d56a220ad13f7104237d57ace4ec86"
        
        
        /// Url format
        static let tagsSearchFormat = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&format=json&nojsoncallback=1&safe_search=1&text=%@&page=%i"
    }
}

extension FlickerServiceManager: FlickrPhotoSearchProtocol {
    //fetches json data from the service
    func fetchPhotosFor(searchText: String, page: Int, completion: @escaping (Result<PhotoObject, ServiceErrors>, Int) -> Void) {
        let escapedSearchText = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        let format = FlickrAPI.tagsSearchFormat
        let arguments: [CVarArg] = [FlickrAPI.APIKey, escapedSearchText!, page]
        
        let photosURL = String(format: format, arguments: arguments)
        
        let url = URL(string: photosURL)!
        let request = URLRequest(url: url)
        Log.console(debug: "URL Called:\(url.absoluteString)")
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let jsonData = data , error == nil  else {
                completion(.failure(ServiceErrors.handlerError(response: response)) , 0)
                return
            }
            
            do {
                
                if let responseData = JSONResponseDecoder<Data, PhotoObject>.decode(jsonData) {
                    completion(.success(responseData), 0)
                }else {
                    completion(.failure(.cannotProcessData(msg:"Cannot Process data")), 0)
                }
                
                
                let resultsDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : AnyObject]
                
                Log.console(debug: "\(resultsDictionary ?? [:])")
                
            } catch let error as NSError {
                print("Error parsing JSON: \(error)")
                completion(.failure(.cannotProcessData(msg:"Cannot Process data")),0)
            }
        }
        
        dataTask.resume()
    }
}
