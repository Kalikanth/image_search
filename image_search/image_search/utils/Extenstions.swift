//
//  Extenstions.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()


extension String {
    //converts string to int
    var toInt: Int {
        return Int(self) ?? 0
    }
    
    //clears the empty spaces
    public func trim() -> String {
        
        return trimmingCharacters(in: .whitespaces)
    }
}

extension UIImageView {
    
    //Downloads image from url
    func downloadImage(with url: URL, completion: @escaping (UIImage? , _ isSucess: Bool) -> ())  {
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            var downloadedImage: UIImage?

            if let data = data {
                downloadedImage = UIImage(data: data)
                if downloadedImage != nil {
                    imageCache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
                }
                DispatchQueue.main.async {
                    self.image = downloadedImage
                    completion(downloadedImage,true)
                }
            }else {
                 completion(downloadedImage,false)
            }
        }

        dataTask.resume()
    }
    
    //checks the image in cahce and gets it from server
    func getImageFrom(url: URL , completion: @escaping (UIImage?) -> ()) {
        if let image = imageCache.object(forKey: url.absoluteString as NSString) {
            self.image = image
        }else {
            downloadImage(with: url) { (_, _) in   }
        }
    }
}

extension UIViewController {
    
    
    /// Shows alert wieht message
    /// - Parameters:
    ///   - title: title of the alert
    ///   - message: message desctiption of the alert
    func showAlert(title:String = "Alert" , message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil) )
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     Method returns the top viewcontroller which is presenting now
     */
    class func topViewController(base: UIViewController? = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
