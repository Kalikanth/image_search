//
//  LogConsoler.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation


/// Logs are logged to console only in debug mode
struct Log {
    static func console(debug log:String) {
        #if DEBUG
        Swift.print("\n\(log)\n")
        #endif
    }
}
