//
//  Encodable+Decodable.swift
//  image_search
//
//  Created by kiran kumar Gajula on 24/06/20.
//  Copyright © 2020 kiran kumar Gajula. All rights reserved.
//

import Foundation

open class GenericDecoder<IN, OUT: Codable> {
    public class func decode(_ data: IN) -> OUT? {
        fatalError("This method is empty please implement it on a subclass")
    }
    
    public class func decodeJSON(_ data: IN) -> OUT? {
        fatalError("This method is empty please implement it on a subclass")
    }
}

open class GeneriEncoder<IN: Encodable> {
    public class func encode(_ object: IN) -> Data? {
        fatalError("This method is empty please implement it on a subclass")
    }
}


//Generic decoder of json to model objsects
class JSONResponseDecoder<IN, OUT: Codable> : GenericDecoder<IN, OUT> {
    
    public override class func decode(_ data: IN) -> OUT? {
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(OUT.self, from: data as! Data)
        } catch let DecodingError.dataCorrupted(context) {
            Log.console(debug: "Context: \(context)")
            return nil
        }
        catch let DecodingError.keyNotFound(key, context) {
            Log.console(debug: "Key '\(key)' not found: \(context.debugDescription)")
            return nil
        } catch let DecodingError.valueNotFound(value, context) {
            Log.console(debug: "Value '\(value)' not found: \(context.debugDescription)")
            Log.console(debug: "codingPath: \(context.codingPath)")
            return nil
        } catch let DecodingError.typeMismatch(type, context)  {
            Log.console(debug: "Type '\(type)' mismatch: \(context.debugDescription)")
            Log.console(debug: "codingPath: \(context.codingPath)")
            return nil
        } catch {
            Log.console(debug: "error: \(error)")
            return nil
        }
    }
}

class JSONResponseEncoder<IN:Codable> : GeneriEncoder<IN> {
    
    public override class func encode(_ object: IN) -> Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(object)
    }
}
